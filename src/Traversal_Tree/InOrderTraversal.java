package Traversal_Tree;

import java.util.ArrayList;
import java.util.List;

public class InOrderTraversal implements Traversal{
	private List<Node> lst;
	public InOrderTraversal(){
		lst = new ArrayList<Node>();
	}
	
	public List<Node> traverse(Node node){
		if(node.getLeft() != null){
			traverse(node.getLeft());
		}
		lst.add(node);
		if(node.getRight() != null){
			traverse(node.getRight());
		}
		return lst;
	}
}
