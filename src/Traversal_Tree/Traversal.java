package Traversal_Tree;

import java.util.List;

public interface Traversal {
	public List<Node> traverse(Node node);
	
}
