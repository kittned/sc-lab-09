package Traversal_Tree;

import java.util.ArrayList;
import java.util.List;

public class PostOrderTraversal implements Traversal{
	private List<Node> lst;
	public PostOrderTraversal(){
		lst = new ArrayList<Node>();
	}
	public List<Node> traverse(Node node){
		if(node.getLeft() != null){
			traverse(node.getLeft());
		}
		
		if(node.getRight() != null){
			traverse(node.getRight());
		}
		lst.add(node);
		
		return lst;
	}
}
