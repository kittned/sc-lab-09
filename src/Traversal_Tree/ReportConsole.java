package Traversal_Tree;
import java.util.List;

public class ReportConsole {
	public void display(Node root,Traversal traversal){
		List<Node> lst = traversal.traverse(root);
		for (Node node: lst){
			System.out.print(node.getValue() + " ");
		}
	}
}
