package Traversal_Tree;

public class TraverseTest {
	public static void main(String[] args){
		Node root = create();
		ReportConsole re = new ReportConsole();
		System.out.print("Traverse with PreOrderTraversal : ");
		re.display(root, new PreOrderTraversal());
		System.out.print("\nTraverse with InOrderTraversal  : ");
		re.display(root, new InOrderTraversal());
		System.out.print("\nTraverse with PostOrderTraversal : ");
		re.display(root, new PostOrderTraversal());
	}
	public static Node create(){
		Node A = new Node("A",null,null);
		Node C = new Node("C",null,null);
		Node E = new Node("E",null,null);
		Node H = new Node("H",null,null);
		
		Node D = new Node("D",C,E);
		Node I = new Node("I",H,null);
		
		Node B = new Node("B",A,D);
		Node G = new Node("G",null,I);
		
		Node F = new Node("F",B,G);
		return F;
	}
}