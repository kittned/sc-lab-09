package Traversal_Tree;

import java.util.ArrayList;
import java.util.List;

public class PreOrderTraversal implements Traversal{
	private List<Node> lst;
	
	public PreOrderTraversal(){
		lst = new ArrayList<Node>();
	}
	
	@Override
	public List<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		lst.add(node);
		if (node.getLeft() != null){
	        traverse (node.getLeft());
	    }
	    
	    if (node.getRight() != null){
	        traverse (node.getRight());
	    }
		return lst;
	}
}
