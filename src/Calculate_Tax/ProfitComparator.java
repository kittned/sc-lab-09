package Calculate_Tax;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{
	
	@Override
	public int compare(Company c1, Company c2) {
		// TODO Auto-generated method stub
		double pro1 = c1.getProfit();
		double pro2 = c2.getProfit();
		if (pro1 < pro2) return -1;
		if (pro1 > pro2) return 1;
		return 0;
	}

}
