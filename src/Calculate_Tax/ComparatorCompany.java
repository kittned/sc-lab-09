package Calculate_Tax;

import java.util.ArrayList;
import java.util.Collections;

public class ComparatorCompany {

	public static void main(String[] args) {
		ArrayList<Company> compa = new ArrayList<Company>();
		compa.add(new Company("BTS", 1250000, 950000));
		compa.add(new Company("MRT", 950000, 470000));
		compa.add(new Company("Air Asia", 3680000, 1350000));
		
		System.out.println("---- Before sort");
		for (Company c : compa) {
			System.out.println(c);
		}
		
		Collections.sort(compa, new EarningComparator());
		System.out.println("\n---- After sort with Income");
		for (Company c : compa) {
			System.out.println(c);
		}
		
		Collections.sort(compa, new ExpenseComparator());
		System.out.println("\n---- After sort with Expense");
		for (Company c : compa) {
			System.out.println(c);
		}

		Collections.sort(compa, new ProfitComparator());
		System.out.println("\n---- After sort with Profit");
		for (Company c : compa) {
			System.out.println(c);
		}
	}

}
