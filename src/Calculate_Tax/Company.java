package Calculate_Tax;

public class Company implements Taxable{
	private String nameCom;
	private double income;
	private double expense;
	private double profit;
	
	public Company(String nameCom,double income, double expense){
		this.nameCom = nameCom;
		this.income = income;
		this.expense = expense;
		this.profit = income - expense;
	}
	
	//Override
	public double getTax(){
		double tax = (income - expense)*0.3;
	return tax;
	}

	public double getIncome() {
		// TODO Auto-generated method stub
		return income;
	}
	
	public double getExpense(){
		return expense;
	}
	
	public double getProfit(){
		double profit = this.income - this.expense;
	return profit;
	}
	
	public String toString() {
		return "Company[Name = "+this.nameCom+", income = "+ this.income +", expense = "+this.expense + ", Profit="+ this.profit + "]";
	}
}