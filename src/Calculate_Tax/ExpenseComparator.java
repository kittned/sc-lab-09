package Calculate_Tax;
import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company>{
	
	@Override
	public int compare(Company c1, Company c2) {
		// TODO Auto-generated method stub
		double ex1 = c1.getExpense();
		double ex2 = c2.getExpense();
		if (ex1 < ex2) return -1;
		if (ex1 > ex2) return 1;
		
		return 0;
	}

}
