package Calculate_Tax;


public class Product implements Taxable,  Comparable<Product>{
	private String nameGoods;
	private double price;
	
	public Product(String nameGoods,double price){
		this.nameGoods = nameGoods;
		this.price = price;
	}
	
	//Override
	public double getTax(){
		double tax = price*0.07;
		return tax;
	}
	
	@Override
	public int compareTo(Product other) {
		if (this.price < other.price ) { return -1; }
		if (this.price > other.price ) { return 1;  }
		return 0;
	}
	
	public String toString() {
		return "Product[Name = "+this.nameGoods+", price = "+ this.price + "]";
	}
}
