package Calculate_Tax;

public interface Taxable {
	double getTax();
}
