package Calculate_Tax;

import java.util.ArrayList;
import java.util.Collections;

public class ComparableTestPerson {

	public static void main(String[] args) {
		ArrayList<Person> per = new ArrayList<Person>();
		per.add(new Person("Nadech", 65900));
		per.add(new Person("Yaya", 32000));
		per.add(new Person("Soviga", 567000));
		per.add(new Person("Somsak", 18000));
		
		System.out.println("---- Before sort");
		for (Person p : per) {
			System.out.println(p);
		}
		
		Collections.sort(per);
		System.out.println("\n---- After sort with Income");
		for (Person p : per) {
			System.out.println(p);
		}
		
	}

}
