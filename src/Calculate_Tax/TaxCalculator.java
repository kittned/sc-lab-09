package Calculate_Tax;
import java.util.ArrayList;

public class TaxCalculator {
		public double sums(ArrayList<Taxable> taxList){
			double sum = 0;
			for(Taxable tax: taxList){
				sum += tax.getTax();
			}
			return sum;
		}
}

