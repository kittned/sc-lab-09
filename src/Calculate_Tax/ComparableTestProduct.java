package Calculate_Tax;

import java.util.ArrayList;
import java.util.Collections;

public class ComparableTestProduct {

	public static void main(String[] args) {
		ArrayList<Product> pro = new ArrayList<Product>();
		pro.add(new Product("Chocolate", 29));
		pro.add(new Product("Steak", 129));
		pro.add(new Product("Noodle", 59));
		pro.add(new Product("Milk", 10));
		
		System.out.println("---- Before sort");
		for (Product p : pro) {
			System.out.println(p);
		}
		
		Collections.sort(pro);
		System.out.println("\n---- After sort with Income");
		for (Product p : pro) {
			System.out.println(p);
		}
		
	}

}
