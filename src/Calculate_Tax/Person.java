package Calculate_Tax;

public class Person implements Taxable, Comparable<Person>{
	private String name;
	private double income;
	
	public Person(String name,double income){
		this.name = name;
		this.income = income;
	}

	@Override
	public double getTax() {
		double tax = 0;
		// TODO Auto-generated method stub
		if(income <= 300000){
			tax = income*0.05;
		return tax;} 
		
		else{ 
			tax = ((income-300000)*0.1);
		return tax; }
	}
	
	@Override
	public int compareTo(Person other) {
		if (this.income < other.income ) { return -1; }
		if (this.income > other.income ) { return 1;  }
		return 0;
	}
	
	public String toString() {
		return "Person[Name = "+this.name+", income = "+ this.income + "]";
	}
}
